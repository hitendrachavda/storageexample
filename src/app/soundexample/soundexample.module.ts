import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SoundexamplePageRoutingModule } from './soundexample-routing.module';

import { SoundexamplePage } from './soundexample.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SoundexamplePageRoutingModule
  ],
  declarations: [SoundexamplePage]
})
export class SoundexamplePageModule {}
