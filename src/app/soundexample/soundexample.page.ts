import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { NativeAudio } from '@ionic-native/native-audio/ngx';
import { Storage } from '@ionic/storage-angular';

@Component({
  selector: 'app-soundexample',
  templateUrl: './soundexample.page.html',
  styleUrls: ['./soundexample.page.scss'],
})
export class SoundexamplePage implements OnInit {
  private _storage: Storage | null = null;
  constructor(private nativeAudio: NativeAudio,public platform: Platform,public storage: Storage) { }

  ngOnInit() {
    this.init().then(val => this.getSelectedSound())


    //  this.platform.ready().then(() => {
    //   console.log("platform ready");
    //   this.nativeAudio.preloadComplex('trackID', 'assets/audio/sample1.mp3', 1, 1, 0).then(function() {
    //     console.log("audio loaded!");
    //     //this.playAudio();
    //    }, function(err) {
    //     console.log("audio failed: " + err);
    //    });
    //  });
  }

  ionViewDidLoad(){

  }
  getSelectedSound(){
    this.platform.ready().then(() => {
      this._storage.get('selectedSound').then(val => {
        console.log('assets/audio/'+val);
        this.nativeAudio.preloadComplex('trackID', 'assets/audio/'+val, 1, 1, 0).then(function() {
              console.log("audio loaded!");

              this.nativeAudio.play('trackID').then(function() {
                console.log("playing audio!");
              }, function(err) {
                console.log("error playing audio: " + err);
             });

          }, function(err) {
              console.log("audio failed: " + err);
          });
      })

    });
  }

  async init() {
    const storage = await this.storage.create();
    this._storage = storage;
  }

  ionViewWillLeave(){
    this.nativeAudio.stop('trackID').then(function(){},function(err){});
    this.nativeAudio.unload('trackID').then(function() {
      console.log("unloaded audio!");
    }, function(err) {
      console.log("couldn't unload audio... " + err);
    });
  }
  playAudio() {
    this.nativeAudio.play('trackID').then(function() {
        console.log("playing audio!");
    }, function(err) {
        console.log("error playing audio: " + err);
    });
  }
}
