import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SoundexamplePage } from './soundexample.page';

const routes: Routes = [
  {
    path: '',
    component: SoundexamplePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SoundexamplePageRoutingModule {}
