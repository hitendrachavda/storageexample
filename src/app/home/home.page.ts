import { Component } from '@angular/core';
import { AttachSession } from 'protractor/built/driverProviders';
import { Storage } from '@ionic/storage-angular';
import { Router } from '@angular/router';
import { NativeAudio } from '@ionic-native/native-audio/ngx';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  private _storage: Storage | null = null;
  sound : boolean = false;
  vibration : boolean = false;
  name : string = ""
  selectedSound : string = "sample1.mp3"
  sounds = [
    'sample1.mp3',
    'sample2.mp3',
    'sample3.mp3'
  ]

  constructor(public storage: Storage,private nativeAudio: NativeAudio,private route: Router) {

  }
  ngOnInit() {
    this.init().then(val => this.getname())

  }
  nextpage() {
    this.route.navigate(['/soundexample']);
  }
  async init() {
    const storage = await this.storage.create();
    this._storage = storage;
  }

  public set(key: string, value: any) {
    this._storage?.set(key, value);
  }
  updateSound(toggle){
     // this.sound = !this.sound
     this.set('sound',toggle.checked)
  }

  updateVibration(toggle){
    //this.vibration = !this.vibration
    this.set('vibration',toggle.checked)
  }
  async clear(){
    await this._storage.clear();
  }

  getname(){

     this._storage.get('sound').then(val => this.sound = val)
     this._storage.get('vibration').then(val => this.vibration = val)
     this._storage.get('selectedSound').then(val => {
       this.selectedSound = val
      //  console.log('assets/audio/'+val);
      //  this.nativeAudio.preloadComplex('trackID', 'assets/audio/'+val, 1, 1, 0).then(function() {
      //       console.log("audio loaded!");
      //       this.playAudio()
      //   }, function(err) {
      //       console.log("audio failed: " + err);
      //   });
      })
     //this.clear();
  }

  playAudio() {
    this.nativeAudio.play('trackID').then(function() {
        console.log("playing audio!");
    }, function(err) {
        console.log("error playing audio: " + err);
    });
}
checkedSound(soundName){
  if(this.selectedSound == soundName){
    return true;
  }
  else{
    return false;
  }
}
  changeMethod(soundName){
    this.selectedSound = soundName
    this.set('selectedSound',soundName)

    this.nativeAudio.unload('trackID').then(function() {
      console.log("unloaded audio!");
    }, function(err) {
      console.log("couldn't unload audio... " + err);
    });

    this.nativeAudio.preloadComplex('trackID', 'assets/audio/'+soundName, 1, 1, 0).then(function() {
        console.log("audio loaded!");
        setTimeout(() => {
          this.playAudio()
        }, 500);
    }, function(err) {
        console.log("audio failed: " + err);
    });
  }
}
